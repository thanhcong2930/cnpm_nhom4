﻿using QLQuanAn.DAO;
using QLQuanAn.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QLQuanAn
{
    /// <summary>
    /// Interaction logic for FAdmin.xaml
    /// </summary>
    public partial class FAdmin : Window
    {
        public FAdmin()
        {
            InitializeComponent();          
            Load();          
        } 

        private event EventHandler myInsert;
        public event EventHandler MyInsert
        {
            add { myInsert += value; }
            remove { myInsert -= value; }
        }

        private event EventHandler myDetele;
        public event EventHandler MyDetele
        {
            add { myDetele += value; }
            remove { myDetele -= value; }
        }

        private event EventHandler myUpdate;
        public event EventHandler MyUpdate
        {
            add { myUpdate += value; }
            remove { myUpdate -= value; }
        }

        private int pageNum;
        public int PageNum { get => pageNum; set => pageNum = value; }

        private int lastPage;
        public int LastPage { get => lastPage; set => lastPage = value; }      

        private void Load()
        {
            LoadDatePickerBill();
            LoadComboxCategoryFood(cbFoodCategory);
            LoadComboxAccountType(cbbAccountType);
        }

        private void LoadPageNum(int page)
        {
            txbPageBill.Text = page.ToString() + " - " + LastPage.ToString();
        }

        private void LoadListFood()
        {
            dtgvFood.ItemsSource = FoodDAO.Instance.GetListFood().DefaultView; 
        }

        private void LoadListFoodCategory()
        {
            dtgvFoodCategory.ItemsSource = CategoryDAO.Instance.GetListCategory();
        }

        private void LoadListTable()
        {
            dtgvTable.ItemsSource = TableDAO.Instance.LoadTableList();
        }

        private void LoadListAccount()
        {
            dtgvAccount.ItemsSource = AccountDAO.Instance.GetListAccount().DefaultView;
        }

        private void LoadComboxCategoryFood(ComboBox cb)
        {
            cb.ItemsSource = CategoryDAO.Instance.GetListCategory();
            cb.DisplayMemberPath = "Name";
        }

        private void LoadComboxAccountType(ComboBox cb)
        {
            cb.ItemsSource = AccountTypeDAO.Instance.GetListAccountType();
            cb.DisplayMemberPath = "TypeName";
        }

        private void LoadDatePickerBill()
        {
            DateTime today = DateTime.Now;
            DateTime firstMonth = new DateTime(today.Year, today.Month, 1);
            dtFromDate.Text = firstMonth.ToShortDateString();
            dtToDate.Text = firstMonth.AddMonths(1).AddDays(-1).ToShortDateString();
        }

        private void LoadListBillByDate(DateTime checkin, DateTime checkout)
        {
            dtgvBill.ItemsSource = BillDAO.Instance.GetListBillByDate(checkin, checkout).DefaultView;
        }

        private void LoadListBillByDateAndPage(DateTime checkin, DateTime checkout, int page)
        {
            dtgvBill.ItemsSource = BillDAO.Instance.GetListBillByDateAndPage(checkin, checkout, page).DefaultView;
        }

        private DataTable SearchFoodByName(string name)
        {
            return FoodDAO.Instance.SearchFoodByName(name);
        }

        private DataTable SearchFoodCategotyByName(string name)
        {
            return CategoryDAO.Instance.SearchCategoryByName(name);
        }

        private DataTable SearchTableByName(string name)
        {
            return TableDAO.Instance.SearchTableByName(name);
        }

        private void BtnViewBill_Click(object sender, RoutedEventArgs e)
        {
            PageNum = 1;
            LastPage = CountLastPage();
            LoadPageNum(PageNum);
            LoadListBillByDateAndPage(DateTime.Parse(dtFromDate.Text), DateTime.Parse(dtToDate.Text), 1);
        }


        private void BtnReport_Click(object sender, RoutedEventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                Report rp = new Report(DateTime.Parse(dtFromDate.Text), DateTime.Parse(dtToDate.Text));
                rp.Show();
            });
        }

        private void DtgvBill_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if(e.PropertyType == typeof(DateTime))
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "dd/MM/yyyy";
            }
        }

        private void BtnViewFood_Click(object sender, RoutedEventArgs e)
        {
            LoadListFood();
        }

        private void BtnAddFood_Click(object sender, RoutedEventArgs e)
        {
            int id = (cbFoodCategory.SelectedItem as Category).ID;
            float price = float.Parse(txbFoodPrice.Text);
            if (FoodDAO.Instance.InsertFood(txbFoodName.Text, id, price))
            {
                Popup.NOTIFY(Properties.Resources.INSERT_SUCCESS);
                LoadListFood();
                if (myInsert != null)
                    myInsert(this, new EventArgs());
            }
            else Popup.NOTIFY_ERROR(Properties.Resources.INSERT_FAIL);
        }

        private void BtnUpdateFood_Click(object sender, RoutedEventArgs e)
        {
            int id = int.Parse(txbFoodID.Text);
            int idcategory = (cbFoodCategory.SelectedItem as Category).ID;
            float price = float.Parse(txbFoodPrice.Text);
            if (FoodDAO.Instance.UpdateFood(id, txbFoodName.Text, idcategory, price))
            {
                Popup.NOTIFY(Properties.Resources.UPDATE_SUCCESS);
                LoadListFood();
                if (myUpdate != null)
                    myUpdate(this, new EventArgs());
            }
            else Popup.NOTIFY_ERROR(Properties.Resources.UPDATE_FAIL);
        }

        private void BtnDeleteFood_Click(object sender, RoutedEventArgs e)
        {
            if (Popup.NOTIFY_WARNING(Properties.Resources.DELETE_NOTIFY_QUESTION + "\n" + Properties.Resources.DELETE_NOTIFY) == MessageBoxResult.Yes)
            {
                int id = int.Parse(txbFoodID.Text);
                if (FoodDAO.Instance.DeleteFood(id))
                {
                    Popup.NOTIFY(Properties.Resources.DELETE_SUCCESS);
                    LoadListFood();
                    if (myDetele != null)
                        myDetele(this, new EventArgs());
                }
                else Popup.NOTIFY_ERROR(Properties.Resources.DELETE_FAIL);
            }
        }

        private void BtnSearchFoodName_Click(object sender, RoutedEventArgs e)
        {
            if (dtgvFood.ItemsSource != null)
                dtgvFood.ItemsSource = SearchFoodByName(txbSearchFoodName.Text).DefaultView;
            else Popup.NOTIFY_ERROR(Properties.Resources.SEARCH_FAIL);
        }

        private void DtgvFood_AutoGeneratedColumns(object sender, EventArgs e)
        {
            (sender as DataGrid).Columns[1].Header = "Tên món";
            (sender as DataGrid).Columns[2].Header = "Danh mục";
            (sender as DataGrid).Columns[3].Header = "Đơn giá (VNĐ)";
        }

        private void TxbFoodID_TextChanged(object sender, TextChangedEventArgs e)
        {
            DataRowView row = (DataRowView)dtgvFood.SelectedItem;
            if (row != null)
            {
                foreach (Category item in cbFoodCategory.Items)
                {
                    if (item.Name == row["Name"].ToString())
                    {
                        cbFoodCategory.SelectedItem = item;
                        break;
                    }
                }
            }
        }

        private void BtnViewFoodCategory_Click(object sender, RoutedEventArgs e)
        {
            LoadListFoodCategory();
        }

        private void BtnAddFoodCategory_Click(object sender, RoutedEventArgs e)
        {
            if (CategoryDAO.Instance.InsertCategory(txbFoodCategoryName.Text))
            {
                Popup.NOTIFY(Properties.Resources.INSERT_SUCCESS);
                LoadListFoodCategory();
                LoadComboxCategoryFood(cbFoodCategory);
                if (myInsert != null)
                    myInsert(this, new EventArgs());
            }
            else Popup.NOTIFY_ERROR(Properties.Resources.INSERT_FAIL);
        }

        private void BtnUpdateFoodCategory_Click(object sender, RoutedEventArgs e)
        {
            int id = int.Parse(txbFoodCategoryID.Text);
            if (CategoryDAO.Instance.UpdateCategory(id, txbFoodCategoryName.Text))
            {
                Popup.NOTIFY(Properties.Resources.UPDATE_SUCCESS);
                LoadListFoodCategory();
                LoadComboxCategoryFood(cbFoodCategory);
                if (myUpdate != null)
                    myUpdate(this, new EventArgs());
            }
            else Popup.NOTIFY_ERROR(Properties.Resources.UPDATE_FAIL);
        }

        private void BtnDeleteFoodCategory_Click(object sender, RoutedEventArgs e)
        {
            if (Popup.NOTIFY_WARNING(Properties.Resources.DELETE_NOTIFY_QUESTION + "\n" + Properties.Resources.DELETE_NOTIFY) == MessageBoxResult.Yes)
            {
                int id = int.Parse(txbFoodCategoryID.Text);
                if (CategoryDAO.Instance.DeleteCategory(id))
                {
                    Popup.NOTIFY(Properties.Resources.DELETE_SUCCESS);
                    LoadListFoodCategory();
                    LoadComboxCategoryFood(cbFoodCategory);
                    if (dtgvFood.ItemsSource != null) LoadListFood();
                    if (myDetele != null)
                        myDetele(this, new EventArgs());
                }
                else Popup.NOTIFY_ERROR(Properties.Resources.DELETE_FAIL);
            }
        }

        private void BtnSearchFoodCategoryName_Click(object sender, RoutedEventArgs e)
        {
            if(dtgvFoodCategory.ItemsSource != null)
                dtgvFoodCategory.ItemsSource = SearchFoodCategotyByName(txbSearchFoodCategoryName.Text).DefaultView;
            else Popup.NOTIFY_ERROR(Properties.Resources.SEARCH_FAIL);
        }

        private void DtgvFoodCategory_AutoGeneratedColumns(object sender, EventArgs e)
        {
            (sender as DataGrid).Columns[1].Header = "Tên danh mục";
        }

        private void BtnViewTable_Click(object sender, RoutedEventArgs e)
        {
            LoadListTable();
        }

        private void BtnAddTable_Click(object sender, RoutedEventArgs e)
        {
            if (TableDAO.Instance.InsertTable(txbTableName.Text))
            {
                Popup.NOTIFY(Properties.Resources.INSERT_SUCCESS);
                LoadListTable();
                if (myInsert != null)
                    myInsert(this, new EventArgs());
            }
            else Popup.NOTIFY_ERROR(Properties.Resources.INSERT_FAIL);
        }

        private void BtnUpdateTable_Click(object sender, RoutedEventArgs e)
        {
            int id = int.Parse(txbTableID.Text);
            if (TableDAO.Instance.UpdateTable(id, txbTableName.Text))
            {
                Popup.NOTIFY(Properties.Resources.UPDATE_SUCCESS);
                LoadListTable();
                if (myUpdate != null)
                    myUpdate(this, new EventArgs());
            }
            else Popup.NOTIFY_ERROR(Properties.Resources.UPDATE_FAIL);
        }

        private void BtnDeleteTable_Click(object sender, RoutedEventArgs e)
        {
            if (Popup.NOTIFY_WARNING(Properties.Resources.DELETE_NOTIFY_QUESTION + "\n" + Properties.Resources.DELETE_NOTIFY) == MessageBoxResult.Yes)
            {
                int id = int.Parse(txbTableID.Text);
                if (TableDAO.Instance.DeleteTable(id))
                {
                    Popup.NOTIFY(Properties.Resources.DELETE_SUCCESS);
                    LoadListTable();
                    if (myDetele != null)
                        myDetele(this, new EventArgs());
                }
                else Popup.NOTIFY_ERROR(Properties.Resources.DELETE_FAIL);
            }
        }

        private void BtnSearchTableName_Click(object sender, RoutedEventArgs e)
        {
            if(dtgvTable.ItemsSource != null)
                dtgvTable.ItemsSource = SearchTableByName(txbSearchTableName.Text).DefaultView;
            else Popup.NOTIFY_ERROR(Properties.Resources.SEARCH_FAIL);
        }

        private void DtgvTable_AutoGeneratedColumns(object sender, EventArgs e)
        {
            (sender as DataGrid).Columns[1].Header = "Tên bàn";
            (sender as DataGrid).Columns[2].Header = "Trạng thái";
        }

        private void BtnViewAccount_Click(object sender, RoutedEventArgs e)
        {
            LoadListAccount();
        }

        private void BtnAddAccount_Click(object sender, RoutedEventArgs e)
        {
            if (!(String.IsNullOrWhiteSpace(txbAccountName.Text) && String.IsNullOrWhiteSpace(txbAccountUserName.Text)))
            {
                int type = (cbbAccountType.SelectedItem as AccountType).ID;
                if (AccountDAO.Instance.InsertAccount(txbAccountUserName.Text, txbAccountName.Text, type))
                {
                    Popup.NOTIFY(Properties.Resources.INSERT_SUCCESS);
                    LoadListAccount();
                    if (myInsert != null)
                        myInsert(this, new EventArgs());
                }
                else Popup.NOTIFY_ERROR(Properties.Resources.INSERT_FAIL);
            }
        }

        private void BtnUpdateAccount_Click(object sender, RoutedEventArgs e)
        {
            int type = (cbbAccountType.SelectedItem as AccountType).ID;
            if (AccountDAO.Instance.UpdateAccountByName(txbAccountUserName.Text, txbAccountName.Text, type))
            {
                Popup.NOTIFY(Properties.Resources.UPDATE_SUCCESS);
                LoadListAccount();
                if (myUpdate != null)
                    myUpdate(this, new EventArgs());
            }
            else Popup.NOTIFY_ERROR(Properties.Resources.UPDATE_FAIL);
        }

        private void BtnDeleteAccount_Click(object sender, RoutedEventArgs e)
        {
            if (Popup.NOTIFY_WARNING(Properties.Resources.DELETE_NOTIFY_QUESTION + "\n" + Properties.Resources.DELETE_NOTIFY) == MessageBoxResult.Yes)
            {
                if (AccountDAO.Instance.DeleteAccount(txbAccountUserName.Text))
                {
                    Popup.NOTIFY(Properties.Resources.DELETE_SUCCESS);
                    LoadListAccount();
                    if (myDetele != null)
                        myDetele(this, new EventArgs());
                }
                else Popup.NOTIFY_ERROR(Properties.Resources.DELETE_FAIL);
            }
        }

        private void DtgvAccount_AutoGeneratedColumns(object sender, EventArgs e)
        {
            (sender as DataGrid).Columns[1].Header = "Tên tài khoản";
            (sender as DataGrid).Columns[2].Header = "Tên hiển thị";
            (sender as DataGrid).Columns[3].Header = "Loại tài khoản";
        }

        private void TxbAccountSTT_TextChanged(object sender, TextChangedEventArgs e)
        {
            DataRowView row = (DataRowView)dtgvAccount.SelectedItem;
            if (row != null)
            {
                foreach (AccountType item in cbbAccountType.Items)
                {
                    if (item.TypeName == row["TypeName"].ToString())
                    {
                        cbbAccountType.SelectedItem = item;
                        break;
                    }
                }
            }
        }

        private void BtnRestPassWord_Click(object sender, RoutedEventArgs e)
        {
            if(Popup.QUESTION(Properties.Resources.REST_PASS) == MessageBoxResult.Yes)
            {
                if(AccountDAO.Instance.RestPassWordAccount(txbAccountUserName.Text))
                    Popup.NOTIFY(Properties.Resources.UPDATE_SUCCESS);
                else Popup.NOTIFY_ERROR(Properties.Resources.DELETE_FAIL);
            }
        }

        private int CountLastPage()
        {
            int MaxPage = BillDAO.Instance.GetMaxPageBillByDate(DateTime.Parse(dtFromDate.Text), DateTime.Parse(dtToDate.Text));
            int LastPage = MaxPage / 5;
            if (MaxPage % 5 != 0) ++LastPage;

            return LastPage;
        }

        private void BtnFirstPageBill_Click(object sender, RoutedEventArgs e)
        {
            LoadPageNum(1);
            LoadListBillByDateAndPage(DateTime.Parse(dtFromDate.Text), DateTime.Parse(dtToDate.Text), 1);
            btnPreviousPageBill.IsEnabled = false;
            btnNextPageBill.IsEnabled = true;
        }

        private void BtnPreviousPageBill_Click(object sender, RoutedEventArgs e)
        {
            if (PageNum > 1) --PageNum;
            LoadPageNum(PageNum);
            LoadListBillByDateAndPage(DateTime.Parse(dtFromDate.Text), DateTime.Parse(dtToDate.Text), PageNum);
            btnNextPageBill.IsEnabled = true;
            if (PageNum <= 1) btnPreviousPageBill.IsEnabled = false;
        }

        private void BtnNextPageBill_Click(object sender, RoutedEventArgs e)
        {
            if (PageNum < LastPage) ++PageNum;
            LoadPageNum(PageNum);
            LoadListBillByDateAndPage(DateTime.Parse(dtFromDate.Text), DateTime.Parse(dtToDate.Text), PageNum);
            btnPreviousPageBill.IsEnabled = true;
            if (PageNum >= LastPage) btnNextPageBill.IsEnabled = false;
        }

        private void BtnLastPageBill_Click(object sender, RoutedEventArgs e)
        {
            LoadPageNum(LastPage);
            LoadListBillByDateAndPage(DateTime.Parse(dtFromDate.Text), DateTime.Parse(dtToDate.Text), LastPage);
            btnNextPageBill.IsEnabled = false;
            btnPreviousPageBill.IsEnabled = true;
        }

        private void TxbPageBill_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Update sau
        }
    }
}
