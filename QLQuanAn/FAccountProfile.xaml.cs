﻿using QLQuanAn.DAO;
using QLQuanAn.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QLQuanAn
{
    /// <summary>
    /// Interaction logic for FAccountProfile.xaml
    /// </summary>
    public partial class FAccountProfile : Window
    {
        public FAccountProfile(Account acc)
        {
            InitializeComponent();

            this.LoginAccount = acc;
        }

        private Account loginAccount;
        public Account LoginAccount
        {
            get { return loginAccount; }
            set { loginAccount = value; LoadAccount(loginAccount); }
        }

        private event EventHandler<AccountEvent> updateAccount;
        public event EventHandler<AccountEvent> UpdateAccount
        {
            add { updateAccount += value; }
            remove { updateAccount -= value; }
        }

        private void LoadAccount(Account acc)
        {
            txbUserName.Text = acc.UserName;
            txbDisplayName.Text = acc.DisplayName;
        }

        private void UpdateAccountInfo()
        {
            if (txbPassWord.Password != "")
            {
                if (txbNewPassWord.Password.Equals(txbReEnterNewPassWord.Password))
                {
                    if (AccountDAO.Instance.UpdateAccount(txbUserName.Text, txbDisplayName.Text, txbPassWord.Password, txbNewPassWord.Password))
                    {
                        Popup.NOTIFY(Properties.Resources.UPDATE_SUCCESS);
                        if (updateAccount != null)
                            updateAccount(this, new AccountEvent(AccountDAO.Instance.GetAccountByUserName(LoginAccount.UserName)));
                    }
                    else Popup.NOTIFY_ERROR(Properties.Resources.UPDATE_FAIL);
                }
                else Popup.NOTIFY_ERROR(Properties.Resources.PASS_UPDATE_INCORECT);
            }
            else Popup.NOTIFY_ERROR(Properties.Resources.LOST_PASS);
        }

        private void BtnExitProfile_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnUpdateProfile_Click(object sender, RoutedEventArgs e)
        {
            UpdateAccountInfo();
        }
    }

    public class AccountEvent : EventArgs
    {
        private Account acc;
        public Account Acc { get => acc; set => acc = value; }

        public AccountEvent(Account _acc)
        {
            this.Acc = _acc;
        }
    }
}
