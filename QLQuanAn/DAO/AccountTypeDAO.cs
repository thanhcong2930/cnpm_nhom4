﻿using QLQuanAn.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLQuanAn.DAO
{
    public class AccountTypeDAO
    {
        private static AccountTypeDAO instance;
        public static AccountTypeDAO Instance
        {
            get { if (AccountTypeDAO.instance == null) AccountTypeDAO.instance = new AccountTypeDAO(); return AccountTypeDAO.instance; }
            private set { AccountTypeDAO.instance = value; }
        }

        private AccountTypeDAO() { }

        public List<AccountType> GetListAccountType()
        {
            List<AccountType> listAccountType = new List<AccountType>();

            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT * FROM dbo.[AccountType]");

            foreach (DataRow item in data.Rows)
                listAccountType.Add(new AccountType(item));

            return listAccountType;
        }
    }
}
