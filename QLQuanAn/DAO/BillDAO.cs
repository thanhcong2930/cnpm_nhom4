﻿using QLQuanAn.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLQuanAn.DAO
{
    public class BillDAO
    {
        private static BillDAO instance;

        public static BillDAO Instance
        {
            get { if (BillDAO.instance == null) BillDAO.instance = new BillDAO(); return BillDAO.instance; }
            private set { BillDAO.instance = value; }
        }

        private BillDAO() { }

        public int GetUncheckBillIDByTableID(int id)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT * FROM dbo.[Bill] WHERE [IdTable] = " + id + " AND [Status] = 0 ");

            if(data.Rows.Count > 0)
            {
                Bill bill = new Bill(data.Rows[0]);
                return bill.ID;
            }

            return -1;
        }

        public int GetMaxIdBill()
        {
            return (int)DataProvider.Instance.ExecuteScalar("SELECT MAX([ID]) FROM dbo.[Bill]");
        }

        public void CheckOut(int id, int discount, float totalprice)
        {
            string query = "UPDATE dbo.[Bill] SET [DateCheckOut] = GETDATE() , [Status] = 1 , [TotalPrice] = " + totalprice + " , [Discount] = " + discount + " WHERE [ID] = " + id;
            DataProvider.Instance.ExecuteNonQuery(query);   
        }

        public void InsertBill(int id)
        {
            DataProvider.Instance.ExecuteNonQuery("EXEC USP_InsertBill @idtable", new object[] { id });
        }

        public DataTable GetListBillByDate(DateTime checkin, DateTime checkout)
        {
            return DataProvider.Instance.ExecuteQuery("EXEC USP_GetListBillByDate @datecheckin , @datacheckout", new object[] { checkin, checkout });
        }

        public DataTable GetListBillByDateAndPage(DateTime checkin, DateTime checkout, int page)
        {
            return DataProvider.Instance.ExecuteQuery("EXEC USP_GetListBillByDateAndPage @datecheckin , @datacheckout , @page", new object[] { checkin, checkout, page });
        }

        public int GetMaxPageBillByDate(DateTime checkin, DateTime checkout)
        {
            return (int)DataProvider.Instance.ExecuteScalar("EXEC USP_GetMaxPageBillByDate @datecheckin , @datacheckout", new object[] { checkin, checkout });
        }
    }
}
