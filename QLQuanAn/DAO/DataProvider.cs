﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLQuanAn.DAO
{
    public class DataProvider
    {
        /// <summary>
        /// Design patern Singleton
        /// </summary>
        private static DataProvider instance;
        public static DataProvider Instance
        {
            get { if (instance == null) instance = new DataProvider(); return DataProvider.instance; }
            private set { DataProvider.instance = value; }
        }

        private DataProvider() { }

        private string connSTR = @"Data Source=.;Initial Catalog=QLQuanAn;Integrated Security=True";     

        public DataTable ExecuteQuery(string query, Object[] parameter = null)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(connSTR))
            {
                try
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(query, conn);

                    if(parameter != null)
                    {
                        string[] listPara = query.Split(' ');
                        int i = 0;
                        foreach (string item in listPara)
                        {
                            if(item.Contains('@'))
                            {
                                cmd.Parameters.AddWithValue(item, parameter[i]);
                                ++i;
                            }
                        }
                    }

                    SqlDataAdapter sqlData = new SqlDataAdapter(cmd);
                    
                    sqlData.Fill(data);

                    conn.Close();

                    return data;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public int ExecuteNonQuery(string query, Object[] parameter = null)
        {
            int data = 0;
            using (SqlConnection conn = new SqlConnection(connSTR))
            {
                try
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(query, conn);

                    if (parameter != null)
                    {
                        string[] listPara = query.Split(' ');
                        int i = 0;
                        foreach (string item in listPara)
                        {
                            if (item.Contains('@'))
                            {
                                cmd.Parameters.AddWithValue(item, parameter[i]);
                                ++i;
                            }
                        }
                    }

                    data = cmd.ExecuteNonQuery();

                    conn.Close();

                    return data;
                }
                catch (Exception)
                {
                    return 0;
                }
            }
        }

        public object ExecuteScalar(string query, Object[] parameter = null)
        {
            object data = 0;
            using (SqlConnection conn = new SqlConnection(connSTR))
            {
                try
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(query, conn);

                    if (parameter != null)
                    {
                        string[] listPara = query.Split(' ');
                        int i = 0;
                        foreach (string item in listPara)
                        {
                            if (item.Contains('@'))
                            {
                                cmd.Parameters.AddWithValue(item, parameter[i]);
                                ++i;
                            }
                        }
                    }

                    data = cmd.ExecuteScalar();

                    conn.Close();

                    return data;
                }
                catch (Exception)
                {
                    return 0;
                }
            }
        }

        public bool ExecuteReader(string query, Object[] parameter = null)
        {
            using (SqlConnection conn = new SqlConnection(connSTR))
            {
                try
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(query, conn);

                    if (parameter != null)
                    {
                        string[] listPara = query.Split(' ');
                        int i = 0;
                        foreach (string item in listPara)
                        {
                            if (item.Contains('@'))
                            {
                                cmd.Parameters.AddWithValue(item, parameter[i]);
                                ++i;
                            }
                        }
                    }

                    SqlDataReader dta = cmd.ExecuteReader();

                    bool result = dta.Read();

                    conn.Close();

                    return result;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}
