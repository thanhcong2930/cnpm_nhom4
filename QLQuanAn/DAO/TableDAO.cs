﻿using QLQuanAn.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLQuanAn.DAO
{
    public class TableDAO
    {
        private static TableDAO instance;

        public static TableDAO Instance
        {
            get { if (TableDAO.instance == null) TableDAO.instance = new TableDAO(); return TableDAO.instance; }
            private set { TableDAO.instance = value; }
        }

        public static double TableWidth = 121.5;
        public static double TableHeight = 121.5;

        private TableDAO() { }

        public List<Table> LoadTableList()
        {
            List<Table> tableList = new List<Table>();

            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetTableList");

            foreach (DataRow item in data.Rows)
                tableList.Add(new Table(item));          

            return tableList;
        }

        public void SwitchTable(int id1, int id2)
        {
            string query = "EXEC USP_SwitchTable @idtable1 , @idtable2";
            DataProvider.Instance.ExecuteQuery(query, new object[] { id1, id2 });
        }

        public DataTable SearchTableByName(string name)
        {
            string query = string.Format("SELECT * FROM dbo.[Table] WHERE dbo.fuConvertToUnsign1([Name]) LIKE N'%' + dbo.fuConvertToUnsign1(N'{0}') + '%'", name);

            return DataProvider.Instance.ExecuteQuery(query);
        }

        public bool InsertTable(string name)
        {
            int result = DataProvider.Instance.ExecuteNonQuery("EXEC USP_InsertTable @name", new object[] { name });

            return result > 0;
        }

        public bool UpdateTable(int id, string name)
        {
            int result = DataProvider.Instance.ExecuteNonQuery("EXEC USP_UpdateTable @id , @name", new object[] { id, name });

            return result > 0;
        }

        public bool DeleteTable(int id)
        {
            string query = "EXEC USP_DeleteTable @id";

            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { id });

            return result > 0;
        }
    }
}
