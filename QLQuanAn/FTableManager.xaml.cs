﻿using QLQuanAn.DAO;
using QLQuanAn.DTO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Text.RegularExpressions;
using Menu = QLQuanAn.DTO.Menu;
using System.Windows.Input;

namespace QLQuanAn
{
    /// <summary>
    /// Interaction logic for FTableManager.xaml
    /// </summary>
    public partial class FTableManager : Window
    {
        public static readonly RoutedCommand toolstripExitCommand = new RoutedUICommand("Options", "ToolstripExitCommand", typeof(FTableManager), new InputGestureCollection(new InputGesture[]
        {
            new KeyGesture(Key.E, ModifierKeys.Control)
        }));

        public static readonly RoutedCommand toolstripCheckOutCommand = new RoutedUICommand("Options", "toolstripCheckOutCommand", typeof(FTableManager), new InputGestureCollection(new InputGesture[]
        {
            new KeyGesture(Key.F, ModifierKeys.Control)
        }));

        public static readonly RoutedCommand toolstripSwitchTableCommand = new RoutedUICommand("Options", "toolstripSwitchTableCommand", typeof(FTableManager), new InputGestureCollection(new InputGesture[]
        {
            new KeyGesture(Key.T, ModifierKeys.Control)
        }));

        public static readonly RoutedCommand toolstripAddFoodCommand = new RoutedUICommand("Options", "toolstripAddFoodCommand", typeof(FTableManager), new InputGestureCollection(new InputGesture[]
        {
            new KeyGesture(Key.I, ModifierKeys.Control)
        }));

        public FTableManager(Account acc)
        {
            InitializeComponent();

            this.LoginAccount = acc;

            ///Thay đổi format định dạng sang vn cho hẳn cái thread 
            CultureInfo culture = new CultureInfo("vi-VN");
            Thread.CurrentThread.CurrentCulture = culture;

            LoadTableList();
            LoadCategoryList();
            LoadComboboxTable(cbSwitchTable);
        }

        private Account loginAccount;
        public Account LoginAccount
        {
            get { return loginAccount; }
            set { loginAccount = value; CheckAccount(loginAccount.Type); }
        }

        private void CheckAccount(int type)
        {
            toolstripAdmin.Visibility = (type == 1) ? Visibility.Visible : Visibility.Hidden;
            toolstripAccoutProfile.Header = LoginAccount.DisplayName;
        }

        private float totalprice = 0;

        private void LoadCategoryList()
        {
            List<Category> listCategory = CategoryDAO.Instance.GetListCategory();
            CbFoodCategory.ItemsSource = listCategory;
            CbFoodCategory.DisplayMemberPath = "Name";
        }

        private void LoadFoodListByIdCategory(int id)
        {
            List<Food> listFood = FoodDAO.Instance.GetListFoodByIdCategory(id);
            CbFood.ItemsSource = listFood;
            CbFood.DisplayMemberPath = "FoodName";
        }

        private void LoadTableList()
        {
            pnlTable.Children.Clear();

            List<Table> tableList = TableDAO.Instance.LoadTableList();

            foreach(Table item in tableList)
            {
                Button btn = new Button() { Width = TableDAO.TableWidth, Height = TableDAO.TableHeight };
                btn.Content = item.Name + Environment.NewLine + item.Status;
                btn.Margin = new Thickness(5);
                btn.Tag = item;
                btn.Click += Btn_Click;  

                switch (item.Status)
                {
                    case "Trống":
                        btn.Background = System.Windows.Media.Brushes.Green;
                        break;
                    default:
                        btn.Background = System.Windows.Media.Brushes.Brown;
                        break;
                }

                pnlTable.Children.Add(btn);
            }
        }

        private void ShowBill(int id)
        {
            lsvBill.Items.Clear();

            totalprice = 0;

            List<Menu> listBillInfo = MenuDAO.Instance.GetListMenuByIdTable(id);

            foreach (Menu item in listBillInfo)
            {
                lsvBill.Items.Add(item);
                totalprice += item.TotalPrice;
            }    

            txbTotalPrice.Text = totalprice.ToString("c");  
        }

        private void LoadComboboxTable(ComboBox cb)
        {
            cb.ItemsSource = TableDAO.Instance.LoadTableList();
            cb.DisplayMemberPath = "Name";
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            int tableID = ((sender as Button).Tag as Table).ID;
            lsvBill.Tag = (sender as Button).Tag;
            
            ShowBill(tableID);
        }

        private void ToolstripLogOut_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ToolstripAccoutProfile_Click(object sender, RoutedEventArgs e)
        {
            FAccountProfile f = new FAccountProfile(LoginAccount);
            f.UpdateAccount += F_UpdateAccount;
            f.ShowDialog();
        }

        private void F_UpdateAccount(object sender, AccountEvent e)
        {
            toolstripAccoutProfile.Header = e.Acc.DisplayName;
            LoginAccount = e.Acc;
        }

        private void ToolstripAdmin_Click(object sender, RoutedEventArgs e)
        {
            FAdmin f = new FAdmin();
            f.MyInsert += F_MyInsert;
            f.MyUpdate += F_MyUpdate;
            f.MyDetele += F_MyDetele;
            f.ShowDialog();
        }

        private void F_MyDetele(object sender, EventArgs e)
        {
            LoadCategoryList();
            LoadTableList();
            LoadComboboxTable(cbSwitchTable);
            if (lsvBill.Tag != null) ShowBill((lsvBill.Tag as Table).ID);
        }

        private void F_MyUpdate(object sender, EventArgs e)
        {
            LoadCategoryList();
            LoadTableList();
            LoadComboboxTable(cbSwitchTable);
            if (lsvBill.Tag != null) ShowBill((lsvBill.Tag as Table).ID);
        }

        private void F_MyInsert(object sender, EventArgs e)
        {
            LoadCategoryList();
            LoadTableList();
            LoadComboboxTable(cbSwitchTable);
        }

        private void ToolstripExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();         
        }

        private void CbFoodCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;

            if (cb.SelectedItem == null) return;

            Category selected = cb.SelectedItem as Category;

            int id = selected.ID;

            LoadFoodListByIdCategory(id);
        }

        private void AddFood()
        {
            if (lsvBill.Tag != null)
            {
                if (int.TryParse(txbFoodCount.Text, out int count))
                {
                    if (Popup.QUESTION(Properties.Resources.ADDFOOD_CONFIRM) == MessageBoxResult.Yes)
                    {
                        Table table = lsvBill.Tag as Table;

                        int idbill = BillDAO.Instance.GetUncheckBillIDByTableID(table.ID);
                        int idfood = (CbFood.SelectedItem as Food).ID;

                        if (idbill == -1)
                        {
                            BillDAO.Instance.InsertBill(table.ID);
                            BillInfoDAO.Instance.InsertBillInfo(BillDAO.Instance.GetMaxIdBill(), idfood, count);
                        }
                        else
                        {
                            BillInfoDAO.Instance.InsertBillInfo(idbill, idfood, count);
                        }

                        txbFoodCount.Text = "1";
                        ShowBill(table.ID);
                        LoadTableList();

                        Popup.NOTIFY(Properties.Resources.ADDFOOD_SUCCESS);
                    }
                }
                else Popup.NOTIFY_ERROR(Properties.Resources.COUNT_INCORECT);
            }
            else Popup.NOTIFY_ERROR(Properties.Resources.TABLE_NOTCHOOSE);
        }

        private void BtnAddFood_Click(object sender, RoutedEventArgs e)
        {
            AddFood();
        }

        private void CheckOut()
        {
            if (lsvBill.Tag != null)
            {
                Table table = lsvBill.Tag as Table;

                int idbill = BillDAO.Instance.GetUncheckBillIDByTableID(table.ID);

                int discount = int.Parse(txbDiscount.Text);

                if (idbill != -1)
                {
                    if (Popup.CHECKOUT(table.Name) == MessageBoxResult.Yes)
                    {
                        totalprice *= (float)(100 - discount) / 100;

                        txbTotalPrice.Text = totalprice.ToString("c");

                        BillDAO.Instance.CheckOut(idbill, discount, totalprice);

                        Popup.NOTIFY(Properties.Resources.SHOWTOTALPRICE + "\n" + txbTotalPrice.Text + "\n" + Properties.Resources.VAT_DISCOUNT);

                        txbFoodCount.Text = "1";
                        txbDiscount.Text = "0";
                        totalprice = 0;

                        ShowBill(table.ID);
                        LoadTableList();
                    }
                }
                else Popup.NOTIFY_ERROR(table.Name + " " + Properties.Resources.NOBILL);
            }
            else Popup.NOTIFY_ERROR(Properties.Resources.TABLE_NOTCHOOSE);
        }

        private void BtnCheckOut_Click(object sender, RoutedEventArgs e)
        {
            CheckOut();
        }

        private void NumberValidationTextBox(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void SwitchTable()
        {
            if (lsvBill.Tag != null)
            {
                int id1 = (lsvBill.Tag as Table).ID;
                int id2 = (cbSwitchTable.SelectedItem as Table).ID;

                if (Popup.QUESTION(Properties.Resources.SWITCH_TABLE + (lsvBill.Tag as Table).Name + " qua " + (cbSwitchTable.SelectedItem as Table).Name) == MessageBoxResult.Yes)
                {
                    TableDAO.Instance.SwitchTable(id1, id2);

                    ShowBill(id1);

                    LoadTableList();
                }
            }
            else Popup.NOTIFY_ERROR(Properties.Resources.TABLE_NOTCHOOSE);
        }

        private void BtnSwitchTable_Click(object sender, RoutedEventArgs e)
        {
            SwitchTable();
        }

        private void ToolstripCheckOut_Click(object sender, RoutedEventArgs e)
        {
            CheckOut();
        }

        private void ToolstripSwitchTable_Click(object sender, RoutedEventArgs e)
        {
            SwitchTable();
        }

        private void ToolstripAddFood_Click(object sender, RoutedEventArgs e)
        {
            AddFood();
        }
    }
}
