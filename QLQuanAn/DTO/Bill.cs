﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLQuanAn.DTO
{
    public class Bill
    {
        private int iD;
        public int ID { get => iD; set => iD = value; }

        private DateTime? dateCheckIn;
        public DateTime? DateCheckIn { get => dateCheckIn; set => dateCheckIn = value; }       

        private DateTime? dateCheckOut;
        public DateTime? DateCheckOut { get => dateCheckOut; set => dateCheckOut = value; }      

        private int idTable;
        public int IdTable { get => idTable; set => idTable = value; }       

        private int status;
        public int Status { get => status; set => status = value; }      

        private int discount;
        public int Discount { get => discount; set => discount = value; }

        public Bill(int id, DateTime datecheckin, DateTime datecheckout, int status, int discount = 0)
        {
            this.ID = id;
            this.DateCheckIn = datecheckin;
            this.dateCheckOut = datecheckout;
            this.Status = status;
            this.Discount = discount;
        }

        public Bill(DataRow row)
        {
            this.ID = (int)row["ID"];
            this.DateCheckIn = (DateTime?)row["DateCheckIn"];
            var dateCheckOutTmp = row["DateCheckOut"];
            if (dateCheckOutTmp.ToString() != "") this.dateCheckOut = (DateTime?)dateCheckOutTmp;
            this.Status = (int)row["Status"];
            this.Discount = (int)row["Discount"];
        }
    }
}
