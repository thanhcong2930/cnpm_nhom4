﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLQuanAn.DTO
{
    public class AccountType
    {
        private int iD;
        public int ID { get => iD; set => iD = value; }      

        private string typeName;
        public string TypeName { get => typeName; set => typeName = value; }

        public AccountType(int id, string name)
        {
            this.ID = id;
            this.TypeName = name;
        }

        public AccountType(DataRow row)
        {
            this.ID = (int)row["ID"];
            this.TypeName = row["TypeName"].ToString();
        }
    }
}
