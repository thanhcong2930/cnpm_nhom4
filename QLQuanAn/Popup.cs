﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace QLQuanAn
{
    public class Popup
    {
        public static MessageBoxResult EXIT()
        {
            return CustomMessageBox.Show(Properties.Resources.EXIT, Properties.Resources.NOTIFY, MessageBoxButton.YesNo, MessageBoxImage.Question);    
        }

        public static MessageBoxResult CHECKOUT(string value)
        {
            return CustomMessageBox.Show(Properties.Resources.CHECKOUT + value + "?", Properties.Resources.NOTIFY, MessageBoxButton.YesNo, MessageBoxImage.Question);
        }

        public static MessageBoxResult QUESTION(string value)
        {
            return CustomMessageBox.Show(value + "?", Properties.Resources.NOTIFY, MessageBoxButton.YesNo, MessageBoxImage.Question);
        }

        public static void LOGIN_FAIL()
        {
            CustomMessageBox.Show(Properties.Resources.LOGIN_FAIL, Properties.Resources.NOTIFY, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public static void NOTIFY(string value)
        {
            CustomMessageBox.Show(value, Properties.Resources.NOTIFY, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public static void NOTIFY_ERROR(string value)
        {
            CustomMessageBox.Show(value, Properties.Resources.NOTIFY, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public static MessageBoxResult NOTIFY_WARNING(string value)
        {
            return CustomMessageBox.Show(value, Properties.Resources.NOTIFY, MessageBoxButton.YesNo, MessageBoxImage.Warning);
        }
    }
}
